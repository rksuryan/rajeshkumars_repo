Plain Working:

SELECT
    *
INTO
    [dataoutpbi]
FROM
    [datain]
	
=======================================================

Select
avg(datain.tsl ) as Tensile,
avg(datain.yld ) as Yield,
avg(datain.dty ) as Density,
avg(datain.flt ) as Flatness,
avg(datain.thk ) as Thickness,
datain.dspl as SensorName,
datain.time as EventTimeStamp
into
[dataoutpbi]
from [datain] TIMESTAMP BY time
Group By TUMBLINGWINDOW(ss,5),datain.dspl, datain.time

Select
avg(datain.tsl ) as Tensile,
avg(datain.yld ) as Yield,
avg(datain.dty ) as Density,
avg(datain.flt ) as Flatness,
avg(datain.thk ) as Thickness,
datain.dspl as SensorName,
datain.time as EventTimeStamp
into
[tblmachinedata]
from [datain] TIMESTAMP BY time
Group By TUMBLINGWINDOW(ss,5),datain.dspl, datain.time

=======================================================

WITH MachineDatum AS (
Select
avg(datain.tsl ) as Tensile,
avg(datain.yld ) as Yield,
avg(datain.dty ) as Density,
avg(datain.flt ) as Flatness,
avg(datain.thk ) as Thickness,
datain.dspl as SensorName,
datain.time as EventTimeStamp
from [datain] TIMESTAMP BY time
Group By TUMBLINGWINDOW(ss,10),datain.dspl, datain.time
)

SELECT * INTO [dataoutpbi] FROM MachineDatum
SELECT * INTO [tblmachinedata] FROM MachineDatum

=======================================================

WITH CompleteMachineDatum AS (
Select
avg(datain.tsl ) as Tensile,
avg(datain.yld ) as Yield,
avg(datain.dty ) as Density,
avg(datain.flt ) as Flatness,
avg(datain.thk ) as Thickness,
datain.dspl as SensorName,
datain.time as EventTimeStamp
from [datain] TIMESTAMP BY time
Group By TUMBLINGWINDOW(ss,1),datain.dspl, datain.time
)

SELECT * INTO [dataoutpbi] FROM CompleteMachineDatum
SELECT * INTO [tensileoutpbi] FROM CompleteMachineDatum
--SELECT * INTO [tblmachinedata] FROM CompleteMachineDatum

=======================================================

WITH
MaterialDatum AS (
Select
avg(datain.tsl ) as Tensile,
avg(datain.yld ) as Yield,
avg(datain.dty ) as Density,
avg(datain.flt ) as Flatness,
avg(datain.thk ) as Thickness,
datain.dspl as SensorName,
datain.time as EventTimeStamp
from [datain] TIMESTAMP BY time
Group By TUMBLINGWINDOW(ss,1),datain.dspl, datain.time
),
PartDatum AS (
Select
avg(datain.wdt ) as PartWidth,
avg(datain.lgt ) as PartLength,
avg(datain.cln ) as PartCenterline,
avg(datain.dtr ) as PartDiameter,
datain.dspl as SensorName,
datain.time as EventTimeStamp
from [datain] TIMESTAMP BY time
Group By TUMBLINGWINDOW(ss,1),datain.dspl, datain.time
)

SELECT * INTO [materialsdata] FROM MaterialDatum
SELECT * INTO [partsdata] FROM PartDatum
--SELECT * INTO [tblmachinedata] FROM CompleteMachineDatum

=======================================================

WITH
MaterialDatum AS (
Select
avg(datain.tsl ) as Tensile,
avg(datain.yld ) as Yield,
avg(datain.dty ) as Density,
avg(datain.flt ) as Flatness,
avg(datain.thk ) as Thickness,
datain.dspl as SensorName,
datain.time as EventTimeStamp
from [datain] TIMESTAMP BY time
Group By TUMBLINGWINDOW(ss,1),datain.dspl, datain.time
),
PartDatum AS (
Select
avg(datain.wdt ) as PartWidth,
avg(datain.lgt ) as PartLength,
avg(datain.cln ) as PartCenterline,
avg(datain.dtr ) as PartDiameter,
datain.dspl as SensorName,
datain.time as EventTimeStamp
from [datain] TIMESTAMP BY time
Group By TUMBLINGWINDOW(ss,1),datain.dspl, datain.time
)
MaterialAndPartDatun As (
Select
avg(datain.tsl ) as Tensile,
avg(datain.yld ) as Yield,
avg(datain.dty ) as Density,
avg(datain.flt ) as Flatness,
avg(datain.thk ) as Thickness,
avg(datain.wdt ) as PartWidth,
avg(datain.lgt ) as PartLength,
avg(datain.cln ) as PartCenterline,
avg(datain.dtr ) as PartDiameter,
datain.dspl as SensorName,
datain.time as EventTimeStamp
from [datain] TIMESTAMP BY time
Group By TUMBLINGWINDOW(ss,1),datain.dspl, datain.time
)
SELECT * INTO [materialsdata] FROM MaterialDatum
SELECT * INTO [partsdata] FROM PartDatum
SELECT * INTO [tblmachinedata] FROM MaterialAndPartDatun

=========================================================

WITH
MaterialDatum AS (
Select
avg(datain.tsl ) as Tensile,
avg(datain.yld ) as Yield,
avg(datain.dty ) as Density,
avg(datain.flt ) as Flatness,
avg(datain.thk ) as Thickness,
datain.dspl as SensorName,
datain.time as EventTimeStamp
from [datain] TIMESTAMP BY time
Group By TUMBLINGWINDOW(ss,1),datain.dspl, datain.time
),
PartDatum AS (
Select
avg(datain.wdt ) as PartWidth,
avg(datain.lgt ) as PartLength,
avg(datain.cln ) as PartCenterline,
avg(datain.dtr ) as PartDiameter,
datain.dspl as SensorName,
datain.time as EventTimeStamp
from [datain] TIMESTAMP BY time
Group By TUMBLINGWINDOW(ss,1),datain.dspl, datain.time
),
MaterialAndPartDatum As (
Select
avg(datain.tsl ) as Tensile,
avg(datain.yld ) as Yield,
avg(datain.dty ) as Density,
avg(datain.flt ) as Flatness,
avg(datain.thk ) as Thickness,
avg(datain.wdt ) as PartWidth,
avg(datain.lgt ) as PartLength,
avg(datain.cln ) as PartCenterline,
avg(datain.dtr ) as PartDiameter,
datain.dspl as SensorName,
datain.time as EventTimeStamp
from [datain] TIMESTAMP BY time
Group By TUMBLINGWINDOW(ss,1),datain.dspl, datain.time
),
MaterialAndPartDatumAggr As (
Select
avg(datain.tsl ) as Tensile,
avg(datain.yld ) as Yield,
avg(datain.dty ) as Density,
avg(datain.flt ) as Flatness,
avg(datain.thk ) as Thickness,
avg(datain.wdt ) as PartWidth,
avg(datain.lgt ) as PartLength,
avg(datain.cln ) as PartCenterline,
avg(datain.dtr ) as PartDiameter,
datain.dspl as SensorName,
datain.time as EventTimeStamp
from [datain] TIMESTAMP BY time
Group By TUMBLINGWINDOW(mi,5),datain.dspl, datain.time
)
SELECT * INTO [materialsdata] FROM MaterialDatum
SELECT * INTO [partsdata] FROM PartDatum
SELECT * INTO [tblmachinedata] FROM MaterialAndPartDatum
SELECT * INTO [materialpartdata] FROM MaterialAndPartDatum
SELECT * INTO [materialpartdataaggr] FROM MaterialAndPartDatumAggr

=========================================================

WITH
MaterialDatum AS (
Select
avg(datain.tsl ) as Tensile,
avg(datain.yld ) as Yield,
avg(datain.dty ) as Density,
avg(datain.flt ) as Flatness,
avg(datain.thk ) as Thickness,
datain.dspl as SensorName,
datain.time as EventTimeStamp
from [datain] TIMESTAMP BY time
Group By TUMBLINGWINDOW(ss,1),datain.dspl, datain.time
),
PartDatum AS (
Select
avg(datain.wdt ) as PartWidth,
avg(datain.lgt ) as PartLength,
avg(datain.cln ) as PartCenterline,
avg(datain.dtr ) as PartDiameter,
datain.dspl as SensorName,
datain.time as EventTimeStamp
from [datain] TIMESTAMP BY time
Group By TUMBLINGWINDOW(ss,1),datain.dspl, datain.time
),
/*
MaterialAndPartDatum As (
Select
avg(datain.tsl ) as Tensile,
avg(datain.yld ) as Yield,
avg(datain.dty ) as Density,
avg(datain.flt ) as Flatness,
avg(datain.thk ) as Thickness,
avg(datain.wdt ) as PartWidth,
avg(datain.lgt ) as PartLength,
avg(datain.cln ) as PartCenterline,
avg(datain.dtr ) as PartDiameter,
datain.dspl as SensorName,
datain.time as EventTimeStamp
from [datain] TIMESTAMP BY time
Group By TUMBLINGWINDOW(ss,1),datain.dspl, datain.time
),
*/
MaterialAndPartDatumAggr As (
Select
avg(datain.tsl ) as Tensile,
avg(datain.yld ) as Yield,
avg(datain.dty ) as Density,
avg(datain.flt ) as Flatness,
avg(datain.thk ) as Thickness,
avg(datain.wdt ) as PartWidth,
avg(datain.lgt ) as PartLength,
avg(datain.cln ) as PartCenterline,
avg(datain.dtr ) as PartDiameter,
datain.dspl as SensorName,
datain.time as EventTimeStamp
from [datain] TIMESTAMP BY time
Group By TUMBLINGWINDOW(minute,1),datain.dspl, datain.time
)
SELECT * INTO [materialsdata] FROM MaterialDatum
SELECT * INTO [partsdata] FROM PartDatum
--SELECT * INTO [tblmachinedata] FROM MaterialAndPartDatum
--SELECT * INTO [materialpartdata] FROM MaterialAndPartDatum
SELECT * INTO [materialpartdataaggr] FROM MaterialAndPartDatumAggr

=========================================================

WITH
MaterialDatum AS (
Select
avg(datain.tsl ) as Tensile,
avg(datain.yld ) as Yield,
avg(datain.dty ) as Density,
avg(datain.flt ) as Flatness,
avg(datain.thk ) as Thickness,
datain.dspl as SensorName,
datain.time as EventTimeStamp
from [datain] TIMESTAMP BY time
Group By TUMBLINGWINDOW(ss,1),datain.dspl, datain.time
),
PartDatum AS (
Select
avg(datain.wdt ) as PartWidth,
avg(datain.lgt ) as PartLength,
avg(datain.cln ) as PartCenterline,
avg(datain.dtr ) as PartDiameter,
datain.dspl as SensorName,
datain.time as EventTimeStamp
from [datain] TIMESTAMP BY time
Group By TUMBLINGWINDOW(ss,1),datain.dspl, datain.time
),
MaterialAndPartDatum As (
Select
avg(datain.tsl ) as Tensile,
avg(datain.yld ) as Yield,
avg(datain.dty ) as Density,
avg(datain.flt ) as Flatness,
avg(datain.thk ) as Thickness,
avg(datain.wdt ) as PartWidth,
avg(datain.lgt ) as PartLength,
avg(datain.cln ) as PartCenterline,
avg(datain.dtr ) as PartDiameter,
datain.dspl as SensorName,
datain.time as EventTimeStamp
from [datain] TIMESTAMP BY time
Group By TUMBLINGWINDOW(ss,1),datain.dspl, datain.time
)
SELECT * INTO [materialsdata] FROM MaterialDatum
SELECT * INTO [partsdata] FROM PartDatum
SELECT * INTO [materialpartdata] FROM MaterialAndPartDatum
--SELECT * INTO [tblmachinedata] FROM MaterialAndPartDatum

