﻿using System;
using System.Xml;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;

namespace IRM.CRMSolution.Plugins
{
    public enum TrackerStatus
    {
        Created = 170520000,
        DateSet = 170520001,
        TaskComplete = 170520002
    }

    public class Common
    {
        public static OrganizationResponse ExecuteAction(IOrganizationService service, string actionName, EntityReference target)
        {
            OrganizationRequest request = new OrganizationRequest()
            {
                RequestName = actionName
            };
            request.Parameters.Add("Target", target);

            return service.Execute(request);
        }

        public static ExecuteWorkflowResponse ExecuteWorkflow(IOrganizationService service, Guid workflowId, Guid entityId)
        {
            ExecuteWorkflowRequest wfRequest = new ExecuteWorkflowRequest()
            {
                WorkflowId = workflowId,
                EntityId = entityId
            };

            return (ExecuteWorkflowResponse)service.Execute(wfRequest);
        }

        public static string GetValueNode(XmlDocument doc, string key)
        {
            XmlNode node = doc.SelectSingleNode(String.Format("Settings/setting[@name='{0}']", key));

            if (node != null)
            {
                return node.SelectSingleNode("value").InnerText;
            }
            return string.Empty;
        }
    }
}
