﻿using System;
using System.Xml;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;


namespace IRM.CRMSolution.Plugins
{
    public class CreateTracker : Plugin
    {
        string sourceFieldName = "";
        string targetFieldName = "";
        string processName = "";
                
        public CreateTracker(string unsecureConfig)
            : base(typeof(CreateTracker))
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(unsecureConfig);
            string pluginEvent = Common.GetValueNode(doc, "event");
            string entityName = Common.GetValueNode(doc, "entity");
            sourceFieldName = Common.GetValueNode(doc, "sourceFieldName");
            targetFieldName = Common.GetValueNode(doc, "targetFieldName");
            processName = Common.GetValueNode(doc, "processName");

            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, pluginEvent, entityName, new Action<LocalPluginContext>(ExecuteCreateTracker)));
           
        }

        protected void ExecuteCreateTracker(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }
            localContext.TracingService.Trace(sourceFieldName);
            localContext.TracingService.Trace(targetFieldName);

            if (localContext.PluginExecutionContext.InputParameters.Contains("Target") && localContext.PluginExecutionContext.InputParameters["Target"] is Entity)
            {
                Entity sourceEntity = (Entity)localContext.PluginExecutionContext.InputParameters["Target"];

                Guid tracker = CreateTaskTracker(localContext.OrganizationService, sourceEntity.Id, sourceEntity.LogicalName);
                localContext.Trace("Tracker: " + tracker.ToString());

            }
        }

        
        private Guid CreateTaskTracker(IOrganizationService service, Guid entityId, string entityName)
        {
            if (IsTaskTrackerExists(service, entityId, entityName, TrackerStatus.Created) == false)
            {
                Entity taskDateSync = new Entity("irm_taskdatesync");
                taskDateSync.Attributes.Add("irm_regardingrecordid", entityId.ToString());
                taskDateSync.Attributes.Add("irm_regardingentityname", entityName);
                taskDateSync.Attributes.Add("irm_sourcefieldname", sourceFieldName);
                taskDateSync.Attributes.Add("irm_targetfieldname", targetFieldName);
                taskDateSync.Attributes.Add("irm_processname", processName);

                return service.Create(taskDateSync);
            }
            else return Guid.Empty;
        }

        private bool IsTaskTrackerExists(IOrganizationService service, Guid entityId, string entityName, TrackerStatus status)
        {
            QueryByAttribute query = new QueryByAttribute("irm_taskdatesync");
            query.ColumnSet = new ColumnSet(false);
            query.AddAttributeValue("statecode", 0); //Active
            query.AddAttributeValue("irm_regardingrecordid", entityId.ToString());
            query.AddAttributeValue("irm_regardingentityname", entityName);
            if (string.IsNullOrEmpty(sourceFieldName) == false)
            {
                query.AddAttributeValue("irm_sourcefieldname", sourceFieldName);
            }
            if (string.IsNullOrEmpty(targetFieldName) == false)
            {
                query.AddAttributeValue("irm_targetfieldname", targetFieldName);
            }
            query.AddAttributeValue("irm_status", (int)status);
            if (string.IsNullOrEmpty(processName) == false)
            {
                query.AddAttributeValue("irm_processname", processName);
            }

            EntityCollection trackers = service.RetrieveMultiple(query);
            if (trackers.Entities.Count > 0) return true;

            return false;
        }
     
       
    }
}
