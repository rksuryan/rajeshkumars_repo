//var _activeStageName = "";
var gRoleName4KBArticle = null;

function form_onload(executionContext) {
	var formContext = executionContext.getFormContext();
	//formContext.getControl("wipfli_statusforclientuser").setVisible(false);

	var currentUserId = Xrm.Utility.getGlobalContext().userSettings.userId;
	var currentUserRoles = ShowHideTaxRequestStatusInfoBasedOnUserRole(executionContext, currentUserId);
}
	
/* function wipfliOnLoad(executionContext){
    //Store current BPF stage name
    if (Xrm.Page.ui.getFormType() == 2) {
        var activeStage = Xrm.Page.data.process.getActiveStage();
        _activeStageName = activeStage.getName();
    }

}

function wipfliOnSave(){
    //force save of sub category in order to fire workflow if BPF stage changed to Research and Analysis
    var activeStage = Xrm.Page.data.process.getActiveStage();
    var activeStageName = activeStage.getName();
    var n1 = activeStageName.localeCompare(_activeStageName);
    var n2 = activeStageName.localeCompare("Research And Analysis");
    if (n1 != 0 && n2 == 0 ) {
        Xrm.Page.getAttribute("wipfli_subcategory").setSubmitMode("always");    
        Xrm.Utility.alertDialog("fire workflow");
        _activeStageName = activeStageName;
    }
} */

// Based on Role allowed, Show KBImportInfo section.
function ShowHideTaxRequestStatusInfoBasedOnUserRole(executionContext, currentUserId){
	var formContext = executionContext.getFormContext();
	var tabName = "tab_AdminInfo";
	
	formContext.ui.tabs.get(tabName).setVisible(false);

	var userId = currentUserId.slice(1, -1);
	var req = new XMLHttpRequest();
	req.open("GET", Xrm.Utility.getGlobalContext().getClientUrl() + "/api/data/v8.2/systemuserrolescollection?$select=roleid&$filter=systemuserid eq "+userId+"", true);
	req.setRequestHeader("OData-MaxVersion", "4.0");
	req.setRequestHeader("OData-Version", "4.0");
	req.setRequestHeader("Accept", "application/json");
	req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
	req.setRequestHeader("Prefer", "odata.include-annotations=\"*\"");
	req.onreadystatechange = function() {
		if (this.readyState === 4) {
			req.onreadystatechange = null;
			if (this.status === 200) {
				var results = JSON.parse(this.response);
				for (var i = 0; i < results.value.length; i++) {
					var userRoleId = results.value[i].roleid;
					GetRoleName(executionContext, userRoleId);
					var userRoleName = gRoleName4KBArticle;
					
					if((userRoleName == "System Customizer") || (userRoleName == "System Administrator"))
					{
						formContext.ui.tabs.get(tabName).setVisible(true);
						break;
					}
				}
			} else {
				Xrm.Navigation.openAlertDialog(this.statusText);
			}
		}
	};
	req.send();
}

//Get Rolename based on RoleId
function GetRoleName(executionContext, roleId) {
	gRoleName4KBArticle = null;

    var req = new XMLHttpRequest();
	req.open("GET", Xrm.Utility.getGlobalContext().getClientUrl() + "/api/data/v8.2/roles("+roleId+")?$select=name", false);
	req.setRequestHeader("OData-MaxVersion", "4.0");
	req.setRequestHeader("OData-Version", "4.0");
	req.setRequestHeader("Accept", "application/json");
	req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
	req.setRequestHeader("Prefer", "odata.include-annotations=\"*\"");
	req.onreadystatechange = function() {
		if (this.readyState === 4) {
			req.onreadystatechange = null;
			if (this.status === 200) {
				var result = JSON.parse(this.response);
				var roleName = result["name"];
				gRoleName4KBArticle = roleName;
			} else {
				Xrm.Navigation.openAlertDialog(this.statusText);
			}
		}
	};
	req.send();
}